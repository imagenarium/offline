#!/bin/bash

#Необходимо указать адрес локального registry
REGISTRY=127.0.0.1:5000 
VERSION=img4

#Выгрузка образов на текущую ноду
docker load < images.tar

#Запуск registry
docker run -d --restart=always -p 5000:5000 -v registry-volume:/var/lib/registry registry:2

#Проверка доступности registry
docker run -e HOST=172.17.0.1 -e PORT=5000 imagenarium/tcpchecker:latest

function push() {
  img=$1
  docker tag $img ${REGISTRY}/$img
  docker push ${REGISTRY}/$img
}

#Перезаливка образов в локальный registry
push imagenarium/authsync:${VERSION}
push imagenarium/clustercontrol:${VERSION}
push imagenarium/dns:${VERSION}
push imagenarium/dockergc:${VERSION}
push imagenarium/standby:${VERSION}
push imagenarium/swarmstorage:${VERSION}
push imagenarium/dswarm:1.0
push imagenarium/docker-socket-proxy:0.1
push imagenarium/fileagent:1.1.4.v3
push imagenarium/conagent:0.4
push imagenarium/runner:0.4
push imagenarium/tcpchecker:latest
push imagenarium/httpchecker:latest

#Проверка
curl http://${REGISTRY}/v2/_catalog