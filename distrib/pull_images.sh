#!/bin/bash

VERSION=img4

echo "Pull images"
docker pull imagenarium/authsync:${VERSION}
docker pull imagenarium/clustercontrol:${VERSION}
docker pull imagenarium/dns:${VERSION}
docker pull imagenarium/dockergc:${VERSION}
docker pull imagenarium/standby:${VERSION}
docker pull imagenarium/swarmstorage:${VERSION}
docker pull imagenarium/dswarm:1.0
docker pull imagenarium/docker-socket-proxy:0.1
docker pull imagenarium/fileagent:1.1.4.v3
docker pull imagenarium/conagent:0.4
docker pull imagenarium/runner:0.4
docker pull imagenarium/tcpchecker:latest
docker pull imagenarium/httpchecker:latest
docker pull registry:2

echo "Save images"
docker save -o images.tar \
imagenarium/authsync:${VERSION} \
imagenarium/clustercontrol:${VERSION} \
imagenarium/dns:${VERSION} \
imagenarium/dockergc:${VERSION} \
imagenarium/standby:${VERSION} \
imagenarium/swarmstorage:${VERSION} \
imagenarium/dswarm:1.0 \
imagenarium/docker-socket-proxy:0.1 \
imagenarium/fileagent:1.1.4.v3 \
imagenarium/conagent:0.4 \
imagenarium/runner:0.4 \
imagenarium/tcpchecker:latest \
imagenarium/httpchecker:latest \
registry:2
