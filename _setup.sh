#!/bin/bash

source /tmp/install.conf &> /dev/null || true
source ./install.conf &> /dev/null || true

echo "========================================================================================"
echo "Configure GRO"
echo "========================================================================================"

if [ ! -f "/etc/default/gro_off.sh" ]; then
  if [[ "${NETWORK_GRO}" == "off" ]]; then
    echo "[Unit]
    Description=GRO Off Service
    After=network.target
    [Service]
    ExecStart=/etc/default/gro_off.sh
    Type=oneshot
    User=root
    [Install]
    WantedBy=multi-user.target" | sed 's/^ *//' > /etc/systemd/system/gro.service

    echo "#!/bin/bash
    ethtool -K ${ADVERTISE_IF} gro off
    ethtool -K ${DATA_PATH_IF} gro off" | sed 's/^ *//' > /etc/default/gro_off.sh

    chmod 755 /etc/default/gro_off.sh
    chmod +x /etc/default/gro_off.sh
    systemctl enable gro && systemctl start gro
  fi
fi
