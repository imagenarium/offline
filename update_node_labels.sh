#!/bin/bash

source ./install.conf
source /etc/imagenarium.conf &> /dev/null || true

: ${DATA_PATH_IF="$ADVERTISE_IF"}
: ${DRBD_IF="$ADVERTISE_IF"}

touch pass.txt

function runsu {
  cat pass.txt | sudo -S $@
}

function runsuRemote {
  sshpass -f ./pass.txt ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -q -p $SSH_PORT $1 "cat /tmp/pass.txt | sudo -S $2"
}

if [[ $(grep -c ^ pass.txt) == "1" ]]; then
  echo "" >> pass.txt
fi

if [ -z "${ADVERTISE_IF}" ]; then
  echo >&2 "ADVERTISE_IF not specified"
  exit -1
fi

for i in ${!nodes[*]} ; do
  echo "========================================================================================"
  echo "Update node labels on node with IP ${nodes[$i]}"
  echo "========================================================================================"

  if [[ $i == "0" ]]; then
    dataPathAddr=$(runsu ip addr show $DATA_PATH_IF | grep 'inet\b' | awk '{print $2}' | cut -d/ -f1)
    drbdAddr=$(runsu ip addr show $DRBD_IF | grep 'inet\b' | awk '{print $2}' | cut -d/ -f1)
    curNode=$(runsu docker info | grep NodeID | head -n1 | awk '{print $2;}')

    echo "Update currente node ${nodes[$i]}: [dataPathAddr: ${dataPathAddr}, drbdAddr: ${drbdAddr}, nodeId: ${curNode}]"
    runsu docker node update --label-add _dataPathAddr=$dataPathAddr $curNode
    runsu docker node update --label-add _drbdAddr=$drbdAddr $curNode
    runsu docker node update --label-add _drbdNodeId=$i $curNode
  else
    sshpass -f ./pass.txt scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -q -P $SSH_PORT "./pass.txt" ${nodes[$i]}:/tmp

    dataPathAddr=$(runsuRemote ${nodes[$i]} "ip addr show $DATA_PATH_IF | grep 'inet\b' | awk '{print \$2}' | cut -d/ -f1")
    drbdAddr=$(runsuRemote ${nodes[$i]} "ip addr show $DRBD_IF | grep 'inet\b' | awk '{print \$2}' | cut -d/ -f1")
    nodeId=$(runsuRemote ${nodes[$i]} "docker info | grep NodeID | head -n1 | awk '{print \$2;}'")

    echo "Update remote node ${nodes[$i]}: [dataPathAddr: ${dataPathAddr}, drbdAddr: ${drbdAddr}, nodeId: ${nodeId}]"
    runsu docker node update --label-add _dataPathAddr=$dataPathAddr $nodeId
    runsu docker node update --label-add _drbdAddr=$drbdAddr $nodeId
    runsu docker node update --label-add _drbdNodeId=$i $nodeId

    runsuRemote ${nodes[$i]} "rm -f /tmp/pass.txt"
  fi
done

runsu rm -f ./pass.txt
