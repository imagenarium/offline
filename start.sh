#!/bin/bash

set +e

source ./setenv.sh
source /etc/imagenarium.conf &> /dev/null || true

if [ ! "$(docker network ls | grep "\sclustercontrol-net\s")" ];then
  echo "Creating network clustercontrol-net..."
  docker network create --driver overlay --attachable clustercontrol-net
fi

if [ ! "$(docker network ls | grep "\sdockersocketproxy-net\s")" ];then
  echo "Creating network dockersocketproxy-net..."
  docker network create --driver overlay --attachable dockersocketproxy-net
fi

export SPRING_PROFILES_ACTIVE=${FEATURES}

#Удаляем метки с других нод
docker node ls --format {{.ID}} | while read -r nodeId
do
  docker node update --label-rm imagenarium $nodeId &> /dev/null || true
done

#Ставим метку на текущую ноду
curNode=$(docker info | grep NodeID | head -n1 | awk '{print $2;}')
docker node update --label-add imagenarium=true $curNode &> /dev/null

dockerRootDir=$(docker info | grep "Docker Root Dir" | awk '{print $4}')
echo "Detected Docker Root Dir: ${dockerRootDir}"

if ! docker config inspect users &> /dev/null; then
  TEMP_PASSWORD=$(echo $RANDOM | base64 | head -c 20; echo)
  echo "Admin users not found. Temporary admin password: ${TEMP_PASSWORD}"
fi

echo "Starting clustercontrol on node: $curNode "
docker service create --name clustercontrol \
--endpoint-mode dnsrr \
--with-registry-auth \
--network clustercontrol-net \
--log-driver=json-file --log-opt max-size=10m --log-opt max-file=10 \
--publish mode=host,target=8080,published=5555 \
--constraint "node.labels.imagenarium == true" \
-e "TEMP_PASSWORD=${TEMP_PASSWORD}" \
-e "SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE}" \
-e "IMAGE_GRACE_PERIOD=${IMAGE_GRACE_PERIOD}" \
-e "CONTAINER_GRACE_PERIOD=${CONTAINER_GRACE_PERIOD}" \
-e "VOLUME_GRACE_PERIOD=${VOLUME_GRACE_PERIOD}" \
-e "JAVA_OPTS=${JAVA_OPTS}" \
-e "AGENT_JAVA_OPTS=${AGENT_JAVA_OPTS}" \
-e "DOCKER_CONFIG_HOME=/root/.docker" \
-e "REGISTRY_PREFIX=${REGISTRY}/" \
-e "OFFLINE=true" \
--container-label "co.elastic.logs/enabled=true" \
--container-label "co.elastic.logs/system=true" \
--container-label "co.elastic.logs/replicas=1" \
--container-label "co.elastic.logs/serviceName=clustercontrol" \
--mount "type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock" \
--mount "type=bind,source=/root/.docker,target=/root/.docker" \
--mount "type=volume,destination=/tmp" \
${REGISTRY}/imagenarium/clustercontrol:${VERSION}

while true; do
  curl -sf http://127.0.0.1:5555 > /dev/null

  status=$?

  if [[ "${status}" == "0" ]]; then
    echo "[IMAGENARIUM]: Done"
    break;
  else
    echo "[IMAGENARIUM]: starting web-console, please wait..."
  fi

  sleep 3
done