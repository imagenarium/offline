#!/bin/bash

source ./install.conf
source /etc/imagenarium.conf &> /dev/null || true

MGR_COUNT=3
: ${DATA_PATH_IF="$ADVERTISE_IF"}
: ${DRBD_IF="$ADVERTISE_IF"}

touch pass.txt

function runsu {
  cat pass.txt | sudo -S $@
}

function runsuRemote {
  sshpass -f ./pass.txt ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -q -p $SSH_PORT $1 "cat /tmp/pass.txt | sudo -S $2"
}

if [[ $(grep -c ^ pass.txt) == "1" ]]; then
  echo "" >> pass.txt
fi

if [ -z "${ADVERTISE_IF}" ]; then
  echo >&2 "ADVERTISE_IF not specified"
  exit -1
fi

for i in ${!nodes[*]} ; do
  echo "========================================================================================"
  echo "Install on node with IP ${nodes[$i]}"
  echo "========================================================================================"

  if [[ $i == "0" ]]; then
    runsu ./_setup.sh

    echo "========================================================================================"
    echo "Initializing first swarm manager ..."
    echo "========================================================================================"

    runsu docker swarm init --listen-addr ${ADVERTISE_IF} --advertise-addr ${ADVERTISE_IF} --data-path-addr ${DATA_PATH_IF}

    dataPathAddr=$(runsu ip addr show $DATA_PATH_IF | grep 'inet\b' | awk '{print $2}' | cut -d/ -f1)
    drbdAddr=$(runsu ip addr show $DRBD_IF | grep 'inet\b' | awk '{print $2}' | cut -d/ -f1)
    curNode=$(runsu docker info | grep NodeID | head -n1 | awk '{print $2;}')

    echo "Update current node ${nodes[$i]}: [dataPathAddr: ${dataPathAddr}, drbdAddr: ${drbdAddr}, nodeId: ${currentNode}]"
    runsu docker node update --label-add _dataPathAddr=$dataPathAddr $curNode
    runsu docker node update --label-add _drbdAddr=$drbdAddr $curNode
    runsu docker node update --label-add _drbdNodeId=$i $curNode

    ManagerToken="`runsu docker swarm join-token manager | grep token` --listen-addr ${ADVERTISE_IF} --advertise-addr ${ADVERTISE_IF} --data-path-addr ${DATA_PATH_IF}"
    WorkerToken="`runsu docker swarm join-token worker | grep token` --listen-addr ${ADVERTISE_IF} --advertise-addr ${ADVERTISE_IF} --data-path-addr ${DATA_PATH_IF}"

    echo ${ManagerToken} > manager_token.sh
    chmod +x manager_token.sh
    echo ${WorkerToken} > worker_token.sh
    chmod +x worker_token.sh
  else
    sshpass -f ./pass.txt scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -q -P $SSH_PORT "./pass.txt" "./_setup.sh" "./manager_token.sh" "./worker_token.sh" "./install.conf" ${nodes[$i]}:/tmp

    runsuRemote ${nodes[$i]} "/tmp/_setup.sh"

    if [[ $i < $MGR_COUNT ]]; then
      runsuRemote ${nodes[$i]} "/tmp/manager_token.sh"
    else
      runsuRemote ${nodes[$i]} "/tmp/worker_token.sh"
    fi

    dataPathAddr=$(runsuRemote ${nodes[$i]} "ip addr show $DATA_PATH_IF | grep 'inet\b' | awk '{print \$2}' | cut -d/ -f1")
    drbdAddr=$(runsuRemote ${nodes[$i]} "ip addr show $DRBD_IF | grep 'inet\b' | awk '{print \$2}' | cut -d/ -f1")
    nodeId=$(runsuRemote ${nodes[$i]} "docker info | grep NodeID | head -n1 | awk '{print \$2;}'")

    echo "Update remote node ${nodes[$i]}: [dataPathAddr: ${dataPathAddr}, drbdAddr: ${drbdAddr}, nodeId: ${nodeId}]"
    runsu docker node update --label-add _dataPathAddr=$dataPathAddr $nodeId
    runsu docker node update --label-add _drbdAddr=$drbdAddr $nodeId
    runsu docker node update --label-add _drbdNodeId=$i $nodeId

    runsuRemote ${nodes[$i]} "rm -f /tmp/pass.txt"
  fi

  echo "========================================================================================"
  echo "Setup Complete on node with IP ${nodes[$i]}"
  echo "========================================================================================"
done

echo "========================================================================================"
echo "Setup completed"
echo "========================================================================================"

runsu docker node ls

runsu rm -f ./manager_token.sh
runsu rm -f ./worker_token.sh
runsu rm -f ./pass.txt
