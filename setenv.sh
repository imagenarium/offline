#!/bin/bash
#
# Внимание!!! Во избежание перезаписи данного файла при обновлении ПО рекомендуется создать
# конфигурационный файл /etc/imagenarium.conf и скопировать настройки туда.
#

# Версия Imagenarium
VERSION="img4"

# gc - удаление неиспользуемых образов, контейнеров, томов данных, конфигов и секретов.
# Рекомендуется отключить эту опцию на продакшене.

# ha - режим повышенной доступности, активируются дополнительные подсистемы.
# На одной ноде это не актуально, до тех пор пока не решите добавить в кластер еще нод. Можно оставить этот режим включенным.
FEATURES="gc,ha"

# specify Docker GC params if GC enabled (min)
IMAGE_GRACE_PERIOD=10080   #7 d
CONTAINER_GRACE_PERIOD=60  #1 h
VOLUME_GRACE_PERIOD=10080  #7 d

# specify heap size for Cluster Control
JAVA_OPTS=-Xmx512m

# specify heap size for agents
AGENT_JAVA_OPTS=-Xmx128m

# specify private registry for Imagenarium distrib
REGISTRY=127.0.0.1:5000
